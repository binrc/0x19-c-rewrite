build: 
	egcc -ggdb -Wall -Wextra -static -o index.cgi index.c
	install -o www -g www -m 0500 index.cgi /var/www/cgi-bin
	#cp posts/* /var/www/cgi-bin/posts/
	chown -R www:www /var/www/cgi-bin/posts

clangbuild: 
	cc -ggdb -Wall -Wextra -static -o index.cgi index.c
	install -o www -g www -m 0500 index.cgi /var/www/cgi-bin
	cp posts/* /var/www/cgi-bin/posts/
	chown -R www:www /var/www/cgi-bin/posts

debug: build
	gdb index.cgi
