#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <string.h>
#include <limits.h>

char *PREFIX = "posts/";
char *FULLPREFIX = "/var/www/cgi-bin/";

void header(char *title){
	puts("<!DOCTYPE html>");
	puts("<html>");
	puts("<head>");
	puts("<style>body{color: white; background-color: black;}</style>");
	printf("<title>%s</title>", title);
	puts("</head>");

	return;
}

void body(char *title, char *body){
	puts("<body>");
	printf("<h1>%s</h1>", title);
	printf("<pre>%s</pre>", body);
	return;
}

void footer(void){
	puts("</body>");
	puts("</html>");
	return;
}

int filecmp(const void *a, const void *b){
	//return ( *(int*) a - *(int*)b); // -> this breaks shit (thanks stack exchange)
	
	// this is the example from qsort(3). It works for strings of the same length as far as I can tell. 
	size_t lena = strlen(*(const char**)a);
	size_t lenb = strlen(*(const char**)b);
	return  lena < lenb ? -1 : lena > lenb;
}


	

int dirlist(char *dirname){
	DIR *dp;
	struct dirent *ep;
	dp = opendir(dirname);

	int nfiles = 0;
	int i = 0;

	if(dp) {
		// count files
		while((ep = readdir(dp)) != NULL){
			if(strcmp("..", ep->d_name) == 0 || strcmp(".", ep->d_name) == 0)
				continue;

			nfiles++;
			// printf("<p><a href='posts/%ls'>%ls</a></p>\n", ep->d_name, ep->d_name); // silence, wench
		}

		// init and load array
		int *files[nfiles];

		rewinddir(dp);

		while((ep = readdir(dp)) != NULL){
			if(strcmp("..", ep->d_name) == 0 || strcmp(".", ep->d_name) == 0)
				continue;

			files[i] = ep->d_name;
			i++;
		}

		// sort, breaks if string is not a unix timestamp
		qsort(files, sizeof(files)/sizeof(files[0]), sizeof(files[0]), filecmp);

		// print file names and their contents, buffered IO
		FILE *fp;
		ssize_t fs= 0;
		int *buf = malloc(0);
		int prefixlen = 6; 
		ssize_t maxpath = _XOPEN_PATH_MAX * sizeof(char);
		char *fullpath = malloc(maxpath+1);

		for(i=0; i<nfiles; i++){
			printf("<p><a href='posts/%ls'>%ls</a></p>\n", files[i], files[i]);
			// build file name
			memset(fullpath,0,maxpath);
			strncpy(fullpath, PREFIX, prefixlen+1);
			strncat(fullpath, files[i], strlen(files[i])+1);
			// printf("%s\n", fullpath); // silence printf debugging
			if((fp = fopen(fullpath, "rb")) == NULL){
					printf("sysadmin is a brokeass without a reliable hard drive");
					continue;
			} 
				fseek(fp, 0, SEEK_END);
				fs = ftell(fp);
				fseek(fp, 0, SEEK_SET);

			if((buf = realloc(buf, fs+1)) == NULL){
				printf("syasdmin is a brokeass without enough ram\n");
				continue;
			}

				fread(buf, fs, 1, fp);
				fclose(fp);
				printf("<pre>");
				fwrite(buf, fs, 1, stdout);
				fflush(stdout);
				printf("</pre>");
					
			}
				
			
		

		closedir(dp);
		return 0;
	} else {
		printf("Couldn't open directory\n");
		return 1;
		}
}




int main(void){
	// allow $DOCROOT/* 
	if(unveil(FULLPREFIX, "r") == -1) {
		perror("unveil() failed: \n");
		exit(1);
	}

	// ban further calls to unveil
	unveil(NULL, NULL);

	// only allow proc to use stdio and access files in posts/
	if(pledge("stdio rpath", NULL) == -1){
		perror("pledge() failed: \n");
		exit(1);
	}


	puts("Status: 200 OK\r");
	puts("Content-Type: text/html\r");
	puts("\r");

	header("index of infohazard");
	body("Infohazard", "Files on this system: ");
	dirlist("./posts");
	footer();

	return 0;
}
